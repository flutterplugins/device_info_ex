// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "DeviceInfoExPlugin.h"
#import <sys/utsname.h>

@implementation FLTDeviceInfoExPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel =
      [FlutterMethodChannel methodChannelWithName:@"plugins.flutter.babybus.sinyee.com/device_info_ex"
                                  binaryMessenger:[registrar messenger]];
  FLTDeviceInfoExPlugin* instance = [[FLTDeviceInfoExPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getIosDeviceInfo" isEqualToString:call.method]) {
    UIDevice* device = [UIDevice currentDevice];
    struct utsname un;
    uname(&un);

    result(@{
      @"name" : [device name],
      @"systemName" : [device systemName],
      @"systemVersion" : [device systemVersion],
      @"model" : [device model],
      @"localizedModel" : [device localizedModel],
      @"identifierForVendor" : [[device identifierForVendor] UUIDString],
      @"isPhysicalDevice" : [self isDevicePhysical],
      @"utsname" : @{
        @"sysname" : @(un.sysname),
        @"nodename" : @(un.nodename),
        @"release" : @(un.release),
        @"version" : @(un.version),
        @"machine" : @(un.machine),
      },
      // EDIT add
      @"deviceType" : @"1",
      @"platform" : @"1",
      @"deviceLang" : @"zh",
      @"appLang" : @"zh",
      @"net" : @"1",
      @"mac" : @"20:00:00:00:00:00",
      @"screen" : @"1920*1080",
      @"bSSID" : @"hhh",
      @"serial" : @"hhhhhhhh",
      @"openID" : @"wrqwtrwertwrt",
      @"iMEI" : @"9872895793845",
      @"jbFlag" : @"0",
      @"iDFA" : @"23452362356",
      @"iDFV" : @"345634634",
      @"rTime" : @"2434",
      @"simIDFA" : @"232345245",
    });
  } else {
    result(FlutterMethodNotImplemented);
  }
}

// return value is false if code is run on a simulator
- (NSString*)isDevicePhysical {
#if TARGET_OS_SIMULATOR
  NSString* isPhysicalDevice = @"false";
#else
  NSString* isPhysicalDevice = @"true";
#endif

  return isPhysicalDevice;
}

@end
