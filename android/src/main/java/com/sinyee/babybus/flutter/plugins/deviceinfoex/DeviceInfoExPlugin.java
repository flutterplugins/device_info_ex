// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.sinyee.babybus.flutter.plugins.deviceinfoex;

import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.io.File;
import java.util.Enumeration;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Locale;

/** DeviceInfoPlugin */
public class DeviceInfoExPlugin implements MethodCallHandler {

  /** Substitute for missing values. */
  private static final String[] EMPTY_STRING_LIST = new String[] {};

  private static Context sContext;

  /** Plugin registration. */
  public static void registerWith(Registrar registrar) {
    sContext = registrar.context();
    final MethodChannel channel =
        new MethodChannel(registrar.messenger(), "plugins.flutter.babybus.sinyee.com/device_info_ex");
    channel.setMethodCallHandler(new DeviceInfoExPlugin());
  }

  /** Do not allow direct instantiation. */
  private DeviceInfoExPlugin() {}

  @Override
  public void onMethodCall(MethodCall call, Result result) {
    if (call.method.equals("getAndroidDeviceInfo")) {
      Map<String, Object> build = new HashMap<>();
      build.put("board", Build.BOARD);
      build.put("bootloader", Build.BOOTLOADER);
      build.put("brand", Build.BRAND);
      build.put("device", Build.DEVICE);
      build.put("display", Build.DISPLAY);
      build.put("fingerprint", Build.FINGERPRINT);
      build.put("hardware", Build.HARDWARE);
      build.put("host", Build.HOST);
      build.put("id", Build.ID);
      build.put("manufacturer", Build.MANUFACTURER);
      build.put("model", Build.MODEL);
      build.put("product", Build.PRODUCT);
      if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
        build.put("supported32BitAbis", Arrays.asList(Build.SUPPORTED_32_BIT_ABIS));
        build.put("supported64BitAbis", Arrays.asList(Build.SUPPORTED_64_BIT_ABIS));
        build.put("supportedAbis", Arrays.asList(Build.SUPPORTED_ABIS));
      } else {
        build.put("supported32BitAbis", Arrays.asList(EMPTY_STRING_LIST));
        build.put("supported64BitAbis", Arrays.asList(EMPTY_STRING_LIST));
        build.put("supportedAbis", Arrays.asList(EMPTY_STRING_LIST));
      }
      build.put("tags", Build.TAGS);
      build.put("type", Build.TYPE);
      build.put("isPhysicalDevice", !isEmulator());

      // EDIT add
      build.put("deviceType", getDeviceType());
      build.put("platform", "11");
      build.put("deviceLang", Locale.getDefault().getLanguage());
      build.put("appLang", Locale.getDefault().getLanguage());
      build.put("net", getNetworkType());
      build.put("mac", getMacAddress());
      build.put("screen", getScreen());
      build.put("bSSID", getBssid());
      build.put("serial", getSerial());
      build.put("openID", getPseudoUniqueID());
      build.put("iMEI", getIMEI());
      build.put("jbFlag", isRoot() ? "1" : "0");
      build.put("iDFA", "");
      build.put("iDFV", "");
      build.put("rTime", "0");
      build.put("simIDFA", "");

      Map<String, Object> version = new HashMap<>();
      if (VERSION.SDK_INT >= VERSION_CODES.M) {
        version.put("baseOS", VERSION.BASE_OS);
        version.put("previewSdkInt", VERSION.PREVIEW_SDK_INT);
        version.put("securityPatch", VERSION.SECURITY_PATCH);
      }
      version.put("codename", VERSION.CODENAME);
      version.put("incremental", VERSION.INCREMENTAL);
      version.put("release", VERSION.RELEASE);
      version.put("sdkInt", VERSION.SDK_INT);
      build.put("version", version);

      result.success(build);
    } else {
      result.notImplemented();
    }
  }

  /**
   * A simple emulator-detection based on the flutter tools detection logic and a couple of legacy
   * detection systems
   */
  private boolean isEmulator() {
    return (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
        || Build.FINGERPRINT.startsWith("generic")
        || Build.FINGERPRINT.startsWith("unknown")
        || Build.HARDWARE.contains("goldfish")
        || Build.HARDWARE.contains("ranchu")
        || Build.MODEL.contains("google_sdk")
        || Build.MODEL.contains("Emulator")
        || Build.MODEL.contains("Android SDK built for x86")
        || Build.MANUFACTURER.contains("Genymotion")
        || Build.PRODUCT.contains("sdk_google")
        || Build.PRODUCT.contains("google_sdk")
        || Build.PRODUCT.contains("sdk")
        || Build.PRODUCT.contains("sdk_x86")
        || Build.PRODUCT.contains("vbox86p")
        || Build.PRODUCT.contains("emulator")
        || Build.PRODUCT.contains("simulator");
  }

  private String getDeviceType() {
      return (Resources.getSystem().getConfiguration().screenLayout
          & Configuration.SCREENLAYOUT_SIZE_MASK)
          >= Configuration.SCREENLAYOUT_SIZE_LARGE ? "2" : "1";
  }

  // region network
  private String getNetworkType() {
        int networkClass = getNetworkClass();
        String type = "UnKnown";
        switch (networkClass) {
            case NETWORK_CLASS_UNAVAILABLE:
                type = "0";
                break;
            case NETWORK_CLASS_WIFI:
                type = "1";
                break;
            case NETWORK_CLASS_2_G:
                type = "2";
                break;
            case NETWORK_CLASS_3_G:
                type = "3";
                break;
            case NETWORK_CLASS_4_G:
                type = "4";
                break;
            case NETWORK_CLASS_UNKNOWN:
                type = "100";
                break;
        }
        return type;
    }

    private int getNetworkClass() {
        int networkType = NETWORK_TYPE_UNKNOWN;
        try {
            final NetworkInfo network = ((ConnectivityManager) sContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE))
                    .getActiveNetworkInfo();
            if (network != null && network.isAvailable()
                    && network.isConnectedOrConnecting()) {
                int type = network.getType();
                if (type == ConnectivityManager.TYPE_WIFI) {
                    networkType = NETWORK_TYPE_WIFI;
                } else if (type == ConnectivityManager.TYPE_MOBILE) {
                    TelephonyManager telephonyManager = (TelephonyManager) sContext.getSystemService(
                            Context.TELEPHONY_SERVICE);
                    networkType = telephonyManager.getNetworkType();
                }
            } else {
                networkType = NETWORK_TYPE_UNAVAILABLE;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return getNetworkClassByType(networkType);

    }

    private int getNetworkClassByType(int networkType) {
        switch (networkType) {
            case NETWORK_TYPE_UNAVAILABLE:
                return NETWORK_CLASS_UNAVAILABLE;
            case NETWORK_TYPE_WIFI:
                return NETWORK_CLASS_WIFI;
            case NETWORK_TYPE_GPRS:
            case NETWORK_TYPE_EDGE:
            case NETWORK_TYPE_CDMA:
            case NETWORK_TYPE_1xRTT:
            case NETWORK_TYPE_IDEN:
                return NETWORK_CLASS_2_G;
            case NETWORK_TYPE_UMTS:
            case NETWORK_TYPE_EVDO_0:
            case NETWORK_TYPE_EVDO_A:
            case NETWORK_TYPE_HSDPA:
            case NETWORK_TYPE_HSUPA:
            case NETWORK_TYPE_HSPA:
            case NETWORK_TYPE_EVDO_B:
            case NETWORK_TYPE_EHRPD:
            case NETWORK_TYPE_HSPAP:
                return NETWORK_CLASS_3_G;
            case NETWORK_TYPE_LTE:
                return NETWORK_CLASS_4_G;
            default:
                return NETWORK_CLASS_UNKNOWN;
        }
    }

    /** Network type is unknown */
    private final int NETWORK_TYPE_UNKNOWN = 0;
    /** Current network is GPRS */
    private final int NETWORK_TYPE_GPRS = 1;
    /** Current network is EDGE */
    private final int NETWORK_TYPE_EDGE = 2;
    /** Current network is UMTS */
    private final int NETWORK_TYPE_UMTS = 3;
    /** Current network is CDMA: Either IS95A or IS95B */
    private final int NETWORK_TYPE_CDMA = 4;
    /** Current network is EVDO revision 0 */
    private final int NETWORK_TYPE_EVDO_0 = 5;
    /** Current network is EVDO revision A */
    private final int NETWORK_TYPE_EVDO_A = 6;
    /** Current network is 1xRTT */
    private final int NETWORK_TYPE_1xRTT = 7;
    /** Current network is HSDPA */
    private final int NETWORK_TYPE_HSDPA = 8;
    /** Current network is HSUPA */
    private final int NETWORK_TYPE_HSUPA = 9;
    /** Current network is HSPA */
    private final int NETWORK_TYPE_HSPA = 10;
    /** Current network is iDen */
    private final int NETWORK_TYPE_IDEN = 11;
    /** Current network is EVDO revision B */
    private final int NETWORK_TYPE_EVDO_B = 12;
    /** Current network is LTE */
    private final int NETWORK_TYPE_LTE = 13;
    /** Current network is eHRPD */
    private final int NETWORK_TYPE_EHRPD = 14;
    /** Current network is HSPA+ */
    private final int NETWORK_TYPE_HSPAP = 15;

    private static final int NETWORK_TYPE_UNAVAILABLE = -1;
    // private static final int NETWORK_TYPE_MOBILE = -100;
    private static final int NETWORK_TYPE_WIFI = -101;

    private static final int NETWORK_CLASS_WIFI = -101;
    private static final int NETWORK_CLASS_UNAVAILABLE = -1;
    /** Unknown network class. */
    private static final int NETWORK_CLASS_UNKNOWN = 0;
    /** Class of broadly defined "2G" networks. */
    private static final int NETWORK_CLASS_2_G = 1;
    /** Class of broadly defined "3G" networks. */
    private static final int NETWORK_CLASS_3_G = 2;
    /** Class of broadly defined "4G" networks. */
    private static final int NETWORK_CLASS_4_G = 3;
    // endregion

  private String getMacAddress() {
      String result = "02:00:00:00:00:00";
      try {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
              // Hardware ID are restricted in Android 6+
              // https://developer.android.com/about/versions/marshmallow/android-6.0-changes.html#behavior-hardware-id
              Enumeration<NetworkInterface> interfaces = null;
              try {
                  interfaces = NetworkInterface.getNetworkInterfaces();
              } catch (SocketException e) {
                  e.printStackTrace();
              }
              while (interfaces != null && interfaces.hasMoreElements()) {
                  NetworkInterface networkInterface = interfaces.nextElement();

                  byte[] addr = new byte[0];
                  try {
                      addr = networkInterface.getHardwareAddress();
                  } catch (SocketException e) {
                      e.printStackTrace();
                  }
                  if (addr == null || addr.length == 0) {
                      continue;
                  }

                  StringBuilder buf = new StringBuilder();
                  for (byte b : addr) {
                      buf.append(String.format("%02X:", b));
                  }
                  if (buf.length() > 0) {
                      buf.deleteCharAt(buf.length() - 1);
                  }
                  String mac = buf.toString();
                  String wifiInterfaceName = "wlan0";
                  result = wifiInterfaceName.equals(networkInterface.getName()) ? mac : result;
              }
          }
          else {
              WifiManager wm = (WifiManager) sContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
              result = wm.getConnectionInfo().getMacAddress();
          }
      } catch(Exception e) {
      }
      return result;
  }

  private String getScreen() {
    return String.format("%s*%s",
       Resources.getSystem().getDisplayMetrics().widthPixels,
       Resources.getSystem().getDisplayMetrics().heightPixels);
  }

  private String getBssid() {
      try {
          WifiManager wm = (WifiManager) sContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
          if (wm == null) {
              return "";
          }
          WifiInfo info = wm.getConnectionInfo();
          if (info == null) {
              return "";
          }
          return TextUtils.isEmpty(info.getBSSID()) ? "" : info.getBSSID();
      } catch(Exception e) {
          return "";
      }
  }

  private String getIMEI() {
      try {
          TelephonyManager telephonyManager = (TelephonyManager) sContext.getSystemService(Context.TELEPHONY_SERVICE);
          return TextUtils.isEmpty(telephonyManager.getDeviceId()) ? "" : telephonyManager.getDeviceId();
      } catch(Exception e) {
        return "";
      }
  }

  /**
    * Gets psuedo unique id.
    *
    * @return the psuedo unique id
    */
  private String getPseudoUniqueID() {
      // If all else fails, if the user does have lower than API 9 (lower
      // than Gingerbread), has reset their phone or 'Secure.ANDROID_ID'
      // returns 'null', then simply the ID returned will be solely based
      // off their Android device information. This is where the collisions
      // can happen.
      // Try not to use DISPLAY, HOST or ID - these items could change.
      // If there are collisions, there will be overlapping data
      String devIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10);

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
          devIDShort += (Build.SUPPORTED_ABIS[0].length() % 10);
      }
      else {
          devIDShort += (Build.CPU_ABI.length() % 10);
      }

      devIDShort +=
              (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length()
                      % 10) + (Build.PRODUCT.length() % 10);

      // Only devices with API >= 9 have android.os.Build.SERIAL
      // http://developer.android.com/reference/android/os/Build.html#SERIAL
      // If a user upgrades software or roots their phone, there will be a duplicate entry
      String serial;
      try {
          serial = Build.SERIAL;

          // Go ahead and return the serial for api => 9
          return new UUID(devIDShort.hashCode(), serial.hashCode()).toString();
      } catch (Exception e) {
          // String needs to be initialized
          serial = "YJU3DW4U"; // some value
      }

      // Finally, combine the values we have found by using the UUID class to create a unique identifier
      return new UUID(devIDShort.hashCode(), serial.hashCode()).toString();
  }

  private String getSerial() {
      // Build.VERSION_CODES.O
      if (Build.VERSION.SDK_INT >= 26) {
          try {
              return Build.SERIAL;
          } catch(Exception e) {
              return "";
          }
      }
      return Build.SERIAL;
  }

  private boolean isRoot() {
      String su = "su";
      String[] locations = {
              "/sbin/", "/system/bin/", "/system/xbin/", "/system/sd/xbin/", "/system/bin/failsafe/",
              "/data/local/xbin/", "/data/local/bin/", "/data/local/"
      };
      for (String location : locations) {
          if (new File(location + su).exists()) {
              return true;
          }
      }
      return false;
  }
}
