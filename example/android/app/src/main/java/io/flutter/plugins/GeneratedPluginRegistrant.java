package io.flutter.plugins;

import io.flutter.plugin.common.PluginRegistry;
import com.sinyee.babybus.flutter.plugins.deviceinfoex.DeviceInfoExPlugin;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
  public static void registerWith(PluginRegistry registry) {
    if (alreadyRegisteredWith(registry)) {
      return;
    }
    DeviceInfoExPlugin.registerWith(registry.registrarFor("com.sinyee.babybus.flutter.plugins.deviceinfoex.DeviceInfoExPlugin"));
  }

  private static boolean alreadyRegisteredWith(PluginRegistry registry) {
    final String key = GeneratedPluginRegistrant.class.getCanonicalName();
    if (registry.hasPlugin(key)) {
      return true;
    }
    registry.registrarFor(key);
    return false;
  }
}
