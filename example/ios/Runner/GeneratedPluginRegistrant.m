//
//  Generated file. Do not edit.
//

#import "GeneratedPluginRegistrant.h"
#import <device_info_ex/DeviceInfoExPlugin.h>

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FLTDeviceInfoExPlugin registerWithRegistrar:[registry registrarForPlugin:@"FLTDeviceInfoExPlugin"]];
}

@end
